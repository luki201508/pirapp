# PIRAPP
---
### Database
* ***USER***
  * id - *Auto generated unique UUID*
  * email - *Unique email* - *Automatic mail sending*
  * name
  * surnames
  * password - *Secure Hashed password*
  * salt - *Salt used for password*